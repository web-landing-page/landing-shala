$(window).on('load', function(){
    
    $('.preloader').fadeOut('slow')
})

$(document).ready(function () {
  // Navbar Shrink
  $(window).on("scroll", function () {
    if ($(this).scrollTop() > 90) {
      $(".navbar").addClass("navbar-shrink");
    } else {
      $(".navbar").removeClass("navbar-shrink");
    }
  });

  // Video Pop Up
  const videoSrc = $("#player-1").attr("src");
  $(".video-play").on("click", function () {
    if ($(".video-popup").hasClass("open")) {
      $(".video-popup").removeClass("open");
      $("#player-1").attr("src", "");
    } else {
      $(".video-popup").addClass("open");
      if ($("#player-1").attr("src") == "") {
        $("#player-1").attr("src", videoSrc);
      }
    }
  });
  // Features Carousel
  $(".features-carousel").owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    autoplay: true,
    responsive: {
      0: {
        items: 1,
        // nav: true
      },
      600: {
        items: 2,
        // nav: false
      },
      1000: {
        items: 3,
        // nav: true
        // loop: false
      },
    },
  });
  // Features Carousel
  $(".screen-images-carousel").owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    autoplay: true,
    responsive: {
      0: {
        items: 1,
        // nav: true
      },
      600: {
        items: 2,
        // nav: false
      },
      1000: {
        items: 3,
        // nav: true
        // loop: false
      },
    },
  });
  // tetsimonials
  $(".testimonials-carousel").owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    autoplay: true,
    responsive: {
      0: {
        items: 1,
        // nav: true
      },
      600: {
        items: 2,
        // nav: false
      },
      1000: {
        items: 3,
        // nav: true
        // loop: false
      },
    },
  });
  // team-carousel
  $(".team-carousel").owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    // autoplay: true,
    responsive: {
      0: {
        items: 1,
        // nav: true
      },
      600: {
        items: 2,
        // nav: false
      },
      1000: {
        items: 3,
        // nav: true
        // loop: false
      },
    },
  });

  // Scroll IT
  $.scrollIt({
    topOffset: -50,
    scrollTime: 300,
  });
  // Navbar collapse

  $(".nav-link").on("click", function () {
    $(".navbar-collapse").collapse("hide");
  });

  // Toogle theme
  function toogleTheme() {
    if (localStorage.getItem("shala-theme") !== null) {
      if (localStorage.getItem("shala-theme") == "dark") {
        $("body").addClass("dark");
      } else {
        $("body").removeClass("dark");
      }
    }
    updateIcon();
  }

  function updateIcon() {
    if ($("body").hasClass("dark")) {
      $(".toogle-theme i").removeClass("fa-moon");
      $(".toogle-theme i").addClass("fa-sun");
    } else {
      $(".toogle-theme i").removeClass("fa-sun");
      $(".toogle-theme i").addClass("fa-moon");
    }
  }

  toogleTheme();

  $(".toogle-theme").on("click", function () {
    // $("body").toogleClass("dark");
    if ($("body").hasClass("dark")) {
      localStorage.setItem("shala-theme", "light");
      $("body").removeClass("dark");
    } else {
      // console.log(" bb");
      $("body").addClass("dark");
      localStorage.setItem("shala-theme", "dark");
    }
    updateIcon()
  });
});
